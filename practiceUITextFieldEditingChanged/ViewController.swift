//
//  ViewController.swift
//  practiceUITextFieldEditingChanged
//
//  Created by 安井春輝 on 11/2/30 H.
//  Copyright © 30 Heisei haruki yasui. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let vietnamTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "vietnam"
        tf.font = UIFont.systemFont(ofSize: 30)
        tf.textAlignment = .center
        tf.backgroundColor = .red
        tf.keyboardType = .numberPad
        tf.keyboardAppearance = UIKeyboardAppearance.dark
        tf.addTarget(self, action: #selector(exchangeMoney(_:)), for: UIControl.Event.editingChanged)
        tf.tag = 1
        return tf
    }()
    
    let japanTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "japan"
        tf.font = UIFont.systemFont(ofSize: 30)
        tf.textAlignment = .center
        tf.backgroundColor = .blue
        tf.keyboardType = .numberPad
        tf.keyboardAppearance = UIKeyboardAppearance.dark
        tf.addTarget(self, action: #selector(exchangeMoney(_:)), for: UIControl.Event.editingChanged)
        tf.tag = 2
        return tf
    }()
    
    @objc func exchangeMoney(_ sender: UIButton) {
        print("selected")
        if sender.tag == 1 {
            guard let vietnamMoney = Int(vietnamTextField.text ?? "") else { return }
            let japanMoney = vietnamMoney / 208
            japanTextField.text = "\(japanMoney)"
            print(japanMoney)
        } else if sender.tag == 2 {
            guard let japanMoney = Int(japanTextField.text ?? "") else { return }
            let vietnamMoney = japanMoney * 208
            vietnamTextField.text = "\(vietnamMoney)"
            print(vietnamMoney)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func setupViews() {
        self.view.addSubview(vietnamTextField)
        vietnamTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        vietnamTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        vietnamTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -50).isActive = true
        vietnamTextField.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        self.view.addSubview(japanTextField)
        japanTextField.topAnchor.constraint(equalTo: vietnamTextField.bottomAnchor, constant: 30).isActive = true
        japanTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        japanTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -50).isActive = true
        japanTextField.heightAnchor.constraint(equalToConstant: 300).isActive = true
    }

}

